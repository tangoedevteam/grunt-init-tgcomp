// NOTE: uncomment the $ funciton parameter and the jQuery parameter at the bottom 
//       if you need jQuery from within your tests

(function(/*$*/) {
  /*
    ======== A Handy Little QUnit Reference ========
    http://api.qunitjs.com/

    Test methods:
      module(name, {[setup][ ,teardown]})
      test(name, callback)
      expect(numberOfAssertions)
      stop(increment)
      start(decrement)
    Test assertions:
      ok(value, [message])
      equal(actual, expected, [message])
      notEqual(actual, expected, [message])
      deepEqual(actual, expected, [message])
      notDeepEqual(actual, expected, [message])
      strictEqual(actual, expected, [message])
      notStrictEqual(actual, expected, [message])
      throws(block, [expected], [message])
  */
  
  // Add your test here ... this is just some dummy test code

  // Let's test this function  
  function isEven(val) {  
      return val % 2 === 0;  
  }  
    
  test('isEven()', function() { 
      ok(isEven(0), 'Zero is an even number'); 
      ok(isEven(2), 'So is two'); 
      ok(isEven(-4), 'So is negative four'); 
      ok(!isEven(1), 'One is not an even number'); 
      ok(!isEven(-7), 'Neither is negative seven');  
  
  
      //ok(isEven(5), '5 is an even number'); 
  });


}(/*jQuery*/));
