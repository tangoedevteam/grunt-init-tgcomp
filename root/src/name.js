/*
 * {%= name %}
 *
 * Copyright (c) {%= grunt.template.today('yyyy') %} Tangoe
 */

(function($) {
	$.widget("tg.{%= name %}", {
		options: {
		},
		
		_create : function() {
			var self = this;
			self._element = $(self.element); 
			
			// add init code here ...
		},
		
		destroy : function() {
			var self = this;
			// cleanup code here ...
			
			$.Widget.prototype.destroy.apply(self, arguments);
		}
	});
})(jQuery);
