# grunt-init-tgcomp

> Create a tgCOMP jQuery plugin with [grunt-init][], including QUnit unit tests.

[grunt-init]: http://gruntjs.com/project-scaffolding

## Installation
If you haven't already done so, install [grunt-init][].

You will also need to install the Karma testing tools in order to perform javascript testing in actual browsers.  
To do this, execute the following:

```
npm install -g karma
```

In addition to Karma, you will need to also install Bower to handle the javascript package management.  
To install Bower, execute the following:

```
npm install -g bower
```

Once grunt-init is installed, place this template in your `~/.grunt-init/` directory. It's recommended that you use git to clone this template into that directory, as follows:

For Windows Users:
```
git clone https://github.com/tgui/grunt-init-tgcomp.git %USERPROFILE%\.grunt-init\tgcomp
```

For OSX or Linux
```
git clone https://github.com/tgui/grunt-init-tgcomp.git ~/.grunt-init/tgcomp
```

## Usage

At the command-line, cd into an empty directory, run this command and follow the prompts.

```
grunt-init tgcomp
```

After you do this, you will need to execute the following commands from the same folder to get all of 
the node and javascript dependencies needed to develop a component.

```
npm install

bower install
```

At this point you will have what you need to develop your component.  To build your component simply execute the following command:

```
grunt
```

This will run the minification, javascript linting, javascript tests and create the javascript components.

If you would like to run the javascript tests in real browsers, then execute the folling comand:

```
grunt browsertest
```



_Note that this template will generate files in the current directory, so be sure to change to a new directory first if you don't want to overwrite existing files._
